# README

Esse repositório possui a solução para as questões presentes no teste de conhecimento proposto pela empresa MOBIT para vaga de Projetista de Sistemas Embarcados. Segue o passo a passo das configurações feitas para o funcionamento correto das soluções propostas.

## Questão 1

As seguintes configurações devem ser realizadas para que o problema seja solucionado. Configuração para que os e-mails possam ser enviados.

### Intalar o POSTFIX
 
Entre no mode root, e instale o POSTFIX
 
```
sudo su 
```
```
apt-get update && apt-get install postfix mailutils
```

Configurar o GMAIL

```
 nano /etc/postfix/sasl_passwd
[smtp.gmail.com]:587    user@gmail.com:password
```

Feche e salve o arquivo, em seguida modifique as permissões de acesso ao arquivo

```
chmod 600 /etc/postfix/sasl_passwd
``` 

Configure o arquivo main.cf

```
 nano /etc/postfix/main.cf
```

```
relayhost = [smtp.gmail.com]:587
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_security_options =
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
```

Feche e salve, depois execute

```
postmap /etc/postfix/sasl_passwd
systemctl restart postfix.service
```

O email precisa estar configurado para permitir o uso de apps menos seguros, veja em:

[Enable "Less Secure Apps" In Gmail](https://support.google.com/accounts/answer/6010255)

### Inserir no arquivo crontab o Script q1_script.sh

Acesse o arquivo /etc/crontab com o editor de texto Nano e incira no final as linhas a seguir, substituindo as informações corretamente:

```
#De 2 em 2 minutos
*/2 * * * * usuario /bin/bash '/caminho_do_arquivo_q1_script/q1_script.sh'
```

Para mais informações sobre como configurar um crontab [veja](https://www.todoespacoonline.com/w/2015/11/cron-e-crontab-no-linux/)

## Questão 3

Antes de gerar o executável com o comando make, entre no arquivo q3.cpp e edite o caminho do arquivo input.xml para que o programa possa encontra-lo.