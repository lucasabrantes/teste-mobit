#!/bin/bash
#Script para monitorar uso das partições do sistema, uso de memoria RAM e temperatura da CPU, informar via e-mail e gerar arquivo de log quando as variaveis monitoradas ultrapassarem os limites estabelecidos: LIMITEUSOPART, LIMITEUSORAM e LIMITETEMPCPU

LIMITEUSOPART=20
LIMITEUSORAM=70
LIMITETEMPCPU=70

#Verifica se arquivo de log ja existe, caso nao cria
if ! [ -e /tmp/log.log ]; then
touch /tmp/log.log
fi

#Arquivo temporario usado para montar mensagem de aviso que sera enviada por e-mail caso as particoes ultrapassem limite LIMITEUSOPART
if [ -e /tmp/usohd.txt ]; then
rm /tmp/usohd.txt
else
touch /tmp/usohd.txt
touch /tmp/df.txt
fi

#Arquivo temporario usado para montar mensagem de aviso que sera enviada por e-mail caso a RAM ultrapassem limite LIMITEUSORAM
if [ -e /tmp/usoram.txt ]; then
rm /tmp/usoram.txt
else touch /tmp/usoram.txt
fi

#Arquivo temporario usado para montar mensagem de aviso que sera enviada por e-mail caso a Temperatura da CPU ultrapassem limite LIMITETEMPCPU
if [ -e /tmp/tempcpu.txt ]; then
rm /tmp/tempcpu.txt
else touch /tmp/tempcpu.txt
fi

#Funcao para verificar o uso das particoes e montar mensagem de aviso
usohd_verify(){
#Verifica a utilizacao, ordena as particões e salvar em arquivo temporario
df -h | grep sda | sort > /tmp/df.txt
# Verifica se as partições estão acima de LIMITEUSOPART
while read linha
do
PARTICAO=`echo $linha | awk '{ print $1 }'`
#Removendo o símbolo %
USOHD=`echo $linha | awk '{ print $5 }' | sed "s/%//g"`
DIRETORIO=`echo $linha | awk '{ print $6 }'`
if [ "$USOHD" -gt "$LIMITEUSOPART" ]; then
echo -e "A partição "$PARTICAO", do diretório "$DIRETORIO" no Servidor "$HOSTNAME" esta com "$USOHD"% de uso! Favor verificar!!!\n" >> /tmp/usohd.txt
fi
done < /tmp/df.txt
}

#Funcao para verificar o uso da memoria RAM e montar mensagem de aviso
usoram_verify(){
USORAM=$(free -m | awk 'NR==2{printf "%d", $3*100/$2 }')
if [ "$USORAM" -gt "$LIMITEUSORAM" ]; then
echo -e "A memória RAM no Servidor "$HOSTNAME" esta com "$USORAM"% de uso! Favor verificar!!!\n" >> /tmp/usoram.txt
fi
}

#Funcao para verificar a temperatura da CPU e montar mensagem de aviso
tempcpu_verify(){
TEMPCPU=$( cat /sys/devices/virtual/thermal/thermal_zone0/temp )
CEL=$'\xc2\xb0C'
TEMPCPU=`expr $TEMPCPU / 1000`
if [ "$TEMPCPU" -gt "$LIMITETEMPCPU" ]; then
echo -e "A temperatura da CPU no Servidor "$HOSTNAME" esta em "$TEMPCPU$CEL"! Favor verificar!!!\n" >> /tmp/tempcpu.txt
fi
}

usohd_verify
usoram_verify
tempcpu_verify

if [ -s /tmp/usohd.txt ]; then
cat /tmp/usohd.txt | mail -s '[Aviso Utilizacao do HD]' lucasabrantesmarques@gmail.com
cat /tmp/usohd.txt 1>&2 >> /tmp/log.log
fi

if [ -s /tmp/usoram.txt ]; then
cat /tmp/usoram.txt | mail -s '[Aviso Utilizacao da RAM]' lucasabrantesmarques@gmail.com
cat /tmp/usoram.txt 1>&2 >> /tmp/log.log
fi

if [ -s /tmp/tempcpu.txt ]; then
cat /tmp/tempcpu.txt | mail -s '[Aviso Temperatura da CPU]' lucasabrantesmarques@gmail.com
cat /tmp/tempcpu.txt 2>&1 >> /tmp/log.log
fi


















