#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define CHANNEL_NUM 3

typedef struct{
	uint8_t * rgb_image;
	int width;
	int height;
	int bpp;
}image_t;

void write_image_length(int socket_fd, int image_length, image_t * p_image, int length){
	int length_int = sizeof(int);
	write(socket_fd, &length_int, sizeof(length_int));
	write(socket_fd, &image_length, length_int);
	printf("tamanho da imagem %d\n", image_length);
	write(socket_fd, p_image, length);
}

void write_image(int socket_fd, image_t * p_image, int length){
	write(socket_fd, &length, sizeof(length));
	write(socket_fd, p_image, length);
}

int main (int argc, char* const argv[]){
	const char* const socket_name = "/tmp/q4_socket";
	int socket_fd;
	struct sockaddr_un name;
	int image_length;
	image_t image;

	image.rgb_image = stbi_load("/home/ubuntu/Imagens/image.png", &image.width, &image.height, &image.bpp, CHANNEL_NUM);
	image_length = image.width * image.height * CHANNEL_NUM;

	socket_fd = socket(PF_LOCAL, SOCK_STREAM, 0);
	name.sun_family = AF_LOCAL;
	strcpy(name.sun_path, socket_name);

	connect(socket_fd, (sockaddr *)&name, SUN_LEN(&name));
	write_image_length(socket_fd, image_length, &image, sizeof(image));
	printf("enviou tamanho\n");

	printf("enviou imagem\n");
	close(socket_fd);

	return 0;
}

