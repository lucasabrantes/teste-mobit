#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define CHANNEL_NUM 3

typedef struct{
	uint8_t * rgb_image;
	int width;
	int height;
	int bpp;
}image_t;

int processo2_get_length (int processo1_socket){
	while(1){
		int length;
		int *p_length_image;
		image_t * p_image;

		if(read(processo1_socket, &length, sizeof(length)) == 0){
			return 0;
		}

		p_length_image = (int *)malloc(length);
		read(processo1_socket, p_length_image, length);
		printf("Tamanho da imagem é %d bytes\n", *p_length_image);


		p_image = (image_t *)malloc(length);
		read(processo1_socket, p_image, length);
		//Por favor definir o caminho correto para a imagem ser salva corretamente
		stbi_write_png("/home/ubuntu/image.png", p_image->width, p_image->height, CHANNEL_NUM, p_image->rgb_image, p_image->width*CHANNEL_NUM);


		free(p_length_image);
		return 1;
	}
}


int main(int argc, char* const argv[]){
	const char* const socket_name = "/tmp/q4_socket";
	int socket_fd;
	struct sockaddr_un name;
	int processo2_sent_quit_message;

	socket_fd = socket(PF_LOCAL, SOCK_STREAM, 0);
	name.sun_family = AF_LOCAL;
	strcpy(name.sun_path, socket_name);
	bind(socket_fd, (sockaddr *)&name, SUN_LEN(&name));
	listen(socket_fd, 5);

	do{
		struct sockaddr_un processo2_name;
		socklen_t processo2_name_len;
		int processo2_socket_fd;

		processo2_socket_fd = accept(socket_fd, (sockaddr *)&processo2_name, &processo2_name_len);
		processo2_sent_quit_message = processo2_get_length(processo2_socket_fd);
		close(processo2_socket_fd);
	}while(!processo2_sent_quit_message);

	close(socket_fd);
	if (unlink(socket_name) != 0) {
		printf("Erro ao tentar remover\n");
		return EXIT_FAILURE;
	}
	printf("Arquivo removido com sucesso.\n");

	return 0;
}

