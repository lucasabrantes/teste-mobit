#include<iostream>
#include <queue>
#include <thread>
#include <mutex>

#include "pugixml.hpp"

using namespace pugi;

std::mutex mtx;
xml_document doc;

static void thread_produtor(std::queue<xpath_node> * p_fila)
{
    mtx.lock();
    xpath_node_set mensagens = doc.select_nodes("/Mensagens/Mensagem");

    for (xpath_node_set::const_iterator it = mensagens.begin(); it != mensagens.end(); ++it)
	{
    	xpath_node node = *it;
		p_fila->push(node);
	}

    mtx.unlock();
}

static void thread_consumidor(std::queue<xpath_node> * p_fila)
{
    mtx.lock();
    while(!p_fila->empty()){
    	xpath_node node = p_fila->front();
    	p_fila->pop();
    	xpath_node payload = node.node().select_node("payload");
    	if(payload){
    		std::cout << payload.node().child_value() << "\n";
    	}
    }
    mtx.unlock();
}

int main()
{
//EDITE O CAMINHO DO ARQUIVO DE ACORDO COM O SEU DIRETORIO
    if (!doc.load_file("/home/ubuntu/teste-mobit/q3/input.xml")){
    	std::cout << "Por favor, verifique se o caminho para o arquivo input.xml esta correto!" << "\n";
    	return -1;
    }

    std::queue<xpath_node> fila;
    std::thread thread_prod(thread_produtor, &fila);
    std::thread thread_cons(thread_consumidor, &fila);

    thread_prod.join();
    thread_cons.join();

}

