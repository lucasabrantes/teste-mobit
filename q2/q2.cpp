#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

#include <syslog.h>

std::mutex mtx;
static const int THREAD_COUNT = 10;

static void print_log(int id)
{
    mtx.lock();
    setlogmask (LOG_UPTO (LOG_NOTICE));
    openlog ("Log_programa_q2", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
    syslog (LOG_NOTICE, "Iniciando bloco %d", id);
    syslog (LOG_NOTICE, "Hello world from thread %d", id);
    syslog (LOG_NOTICE, "Fim do bloco %d", id);
    closelog();
    mtx.unlock();
}


int main()
{
    std::vector<std::thread> v;

    for (size_t i = 0; i < THREAD_COUNT; i++)
    {
        v.emplace_back(print_log, i);
    }

    for (auto &t : v)
    {
        t.join();
    }

    return 0;
}
